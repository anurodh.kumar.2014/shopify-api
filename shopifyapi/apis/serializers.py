from .models import Shop,getShop
from rest_framework import serializers

class ShopifySerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = '__all__'

class getShopifySerializer(serializers.ModelSerializer):
    class Meta:
        model = getShop
        fields = '__all__'